extends Control

const MidiPlayer = preload( "res://addons/midi/MidiPlayer.gd" )
const Utility = preload( "res://addons/midi/Utility.gd" )

var channel:MidiPlayer.GodotMIDIPlayerChannelStatus = null
var midi_player:MidiPlayer = null
var keys:Array[Sprite2D] = []
var midi_player_change:bool = false

func _ready( ):
	for keyboard in $Keyboards.get_children( ):
		for k in keyboard.get_node( "Keys" ).get_children( ):
			self.keys.append( k )

func _process( _delta:float ):
	if self.channel == null: return

	# Channel Number
	$Channel.text = "%02d" % self.channel.number

	# Program Name
	$Program.text = "%03d %s" % [self.channel.program, Utility.program_names[self.channel.program]]

	# Update controls
	self.midi_player_change = true
	$Controls/First/Volume.value = self.channel.volume
	$Controls/First/Expression.value = self.channel.expression
	$Controls/First/Reverb.value = self.channel.reverb
	$Controls/First/Chorus.value = self.channel.chorus
	$Controls/Second/Pan.value = self.channel.pan
	$Controls/Second/PitchBend.value = self.channel.pitch_bend
	$Controls/Second/PitchBendSensitivity.value = self.channel.rpn.pitch_bend_sensitivity
	$Controls/Second/Modulation.value = self.channel.modulation
	$Hold1.button_pressed = self.channel.hold
	self.midi_player_change = false

	# Chord
	var analyse_result = Utility.get_chord_and_scale( channel.note_on.keys( ) )
	if analyse_result != null:
		$Chord.text = analyse_result.string
	else:
		$Chord.text = ""

	# Key display
	for key in self.keys: key.visible = false
	for note in self.channel.note_on.keys( ):
		if note != null:
			self.keys[note].visible = true

func _on_Channel_toggled( button_pressed:bool ):
	if self.midi_player_change: return
	self.channel.mute = button_pressed

func _on_Channel_gui_input( event:InputEvent ):
	if self.midi_player_change: return
	if not ( event is InputEventMouseButton ): return

	var mouse_event:InputEventMouseButton = event
	if mouse_event.button_index == MOUSE_BUTTON_RIGHT and mouse_event.doubleclick:
		for check in self.get_tree( ).get_nodes_in_group( "channel-switch" ):
			if check != self:
				check.button_pressed = true
		$Channel.button_pressed = false

func _on_Volume_value_changed( value:float ):
	if self.midi_player_change: return
	self.channel.volume = value
	self.midi_player.update_channel_status( self.channel )

func _on_Expression_value_changed( value:float ):
	if self.midi_player_change: return
	self.channel.expression = value
	self.midi_player.update_channel_status( self.channel )

func _on_Reverb_value_changed( value:float ):
	if self.midi_player_change: return
	self.channel.reverb = value
	self.midi_player.update_channel_status( self.channel )

func _on_Chorus_value_changed( value:float ):
	if self.midi_player_change: return
	self.channel.chorus = value
	self.midi_player.update_channel_status( self.channel )

func _on_Pan_value_changed( value:float ):
	if self.midi_player_change: return
	self.channel.pan = value
	self.midi_player.update_channel_status( self.channel )

func _on_PitchBend_value_changed( value:float ):
	if self.midi_player_change: return
	self.channel.pitch_bend = value
	self.midi_player.update_channel_status( self.channel )

func _on_PitchBendSensitivity_value_changed( value:float ):
	if self.midi_player_change: return
	self.channel.rpn.pitch_bend_sensitivity = value
	self.midi_player.update_channel_status( self.channel )

func _on_Modulation_value_changed( value:float ):
	if self.midi_player_change: return
	self.channel.modulation = value
	self.midi_player.update_channel_status( self.channel )

func _on_Hold1_toggled( button_pressed:bool ):
	if self.midi_player_change: return
	self.channel.hold = button_pressed
