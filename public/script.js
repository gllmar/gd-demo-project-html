document.addEventListener('DOMContentLoaded', () => {
    const toggleButton = document.getElementById('toggleDarkMode');
    const bodyElement = document.body;

    // Initialize dark mode from localStorage
    initDarkMode();

    toggleButton.addEventListener('click', () => {
        bodyElement.classList.toggle('dark');
        updateLocalStorage();
    });

    // Fetch and display directory data
    fetch('data.json')
        .then(response => response.json())
        .then(data => {
            const projectListContainer = document.getElementById('projectList');
            createLinks(projectListContainer, data);
        })
        .catch(error => console.error('Error loading data:', error));
});

function initDarkMode() {
    if (localStorage.getItem('darkMode') === 'enabled') {
        document.body.classList.add('dark');
        document.getElementById('toggleDarkMode').textContent = 'Disable Dark Mode';
    } else {
        document.body.classList.remove('dark');
        document.getElementById('toggleDarkMode').textContent = 'Enable Dark Mode';
    }
}

function updateLocalStorage() {
    if (document.body.classList.contains('dark')) {
        localStorage.setItem('darkMode', 'enabled');
        document.getElementById('toggleDarkMode').textContent = 'Disable Dark Mode';
    } else {
        localStorage.removeItem('darkMode');
        document.getElementById('toggleDarkMode').textContent = 'Enable Dark Mode';
    }
}

function createLinks(container, data) {
    const list = document.createElement('ul');
    data.forEach(entry => {
        if (entry.files && entry.files.includes('index.html')) {
            const li = document.createElement('li');
            const link = document.createElement('a');
            link.href = entry.path + '/index.html';
            link.textContent = entry.path + '/index.html';
            li.appendChild(link);
            list.appendChild(li);
        }
    });
    container.appendChild(list);
}
