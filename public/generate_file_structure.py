import os
import json
import sys
import fnmatch

def list_files(startpath, ignore_patterns):
    tree = []
    startpath = os.path.abspath(startpath)  # Ensure startpath is absolute
    for root, dirs, files in os.walk(startpath):
        # Filter directories in-place
        dirs[:] = [d for d in dirs if not any(fnmatch.fnmatch(d, pattern) for pattern in ignore_patterns)]

        level = len(root.split(os.sep)) - len(startpath.split(os.sep))
        path = os.path.relpath(root, startpath)  # Get the relative path
        subdir = {'level': level, 'subfolder': os.path.basename(root), 'path': path, 'files': []}

        # Filter files according to ignore patterns
        filtered_files = [file for file in files if not any(fnmatch.fnmatch(file, pattern) for pattern in ignore_patterns)]
        subdir['files'].extend(filtered_files)

        tree.append(subdir)
    
    return tree

def main(directory, output_file):
    ignore_patterns = ['.DS_Store', '*.tmp', '__pycache__']  # Additional patterns can be added here
    data = list_files(directory, ignore_patterns)
    with open(output_file, 'w') as outfile:
        json.dump(data, outfile, indent=4)

    print(f"Data generated and written to {output_file}:")
    print(json.dumps(data, indent=4))  # Print data for debugging

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: python generate_file_structure.py <directory> <output_file>")
        sys.exit(1)
    directory = sys.argv[1]
    output_file = sys.argv[2]
    main(directory, output_file)
